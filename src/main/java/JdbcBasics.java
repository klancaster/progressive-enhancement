import java.sql.*;
import java.util.Arrays;
import java.util.List;

/**
 * Simple class to access a database using JDBC
 */

public class JdbcBasics {
    private static final String INSERT_STATE = "INSERT INTO states(name) VALUES(?)";
    private static final String DB_URL = "jdbc:h2:./target/jdbc-basics;AUTO_SERVER=true";
    private static final String username = "sa";
    private static final String password = "1234";

    public static void main(String[] args) throws SQLException {

        System.out.println("JDBC Basics");

        Connection connection = DriverManager.getConnection(DB_URL,username,password);
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_STATE);
        List<String> names = Arrays.asList("Texas","Oklahoma","Washington","Kansas","Tennessee");
        // TODO: Convert this code to use explicitly use  lambda expression syntax
        for(String name : names){
            preparedStatement.setString(1, name);
            preparedStatement.execute();
        }
        Statement statement = connection.createStatement();
        // TODO: modify this code to only return states that start with "Te"
        ResultSet resultSet = statement.executeQuery("SELECT * FROM states");
        while (resultSet.next()) {
            System.out.println(resultSet.getString("name"));
        }

    }
}
